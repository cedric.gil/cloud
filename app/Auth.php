<?php

/**
 * Class Auth
 */
class Auth
{

    /**
     * Singleton
     * @var Auth
     */
    private static $_instance;

    /**
     * @var Database
     */
    private $db;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param Database $db
     * @return Auth
     */
    public static function getInstance($db){
        if(is_null(self::$_instance)){
            self::$_instance = new Auth($db);
        }
        return self::$_instance;
    }

    /**
     * Auth constructor.
     * @param Database $db
     */
    private function __construct($db){
        $this->db = $db;
        $this->config = $db->config;
    }

    /**
     * @param string $user
     * @param string $password
     * @param string $email
     * Vérifie si l'utilisateur peut être créé, puis le crée
     * @return bool|string
     */
    public function register($user, $email, $password){
        if($user != $password){
            if(strlen($password) >= 8 && preg_match('/[A-Z]/', $password) && preg_match('/[0-9]/', $password)){
                $password = hash('SHA512', $password.$this->config->get('gds'));
                $user = strtolower($user);
                $email = strtolower($email);
                $this->db->checkInjection([$user, $email, $password]);
                if($this->exist(['user' => $user, 'email' => $email]) === false){
                    $this->db->insert("INSERT INTO users (user, email, password) VALUES (?, ?, ?)", [$user, $email, $password]);
                    return true;
                }
                else{
                    return 'exist';
                }
            }
            else{
                return 'low';
            }
        }
        else{
            return 'same';
        }
    }

    /**
     * @param array $array
     * Vérifie si une information existe déjà
     * @return bool
     */
    private function exist($array){
        $query = "SELECT * FROM users WHERE";
        foreach($array as $key => $value){
            if(isset($or)){
                $query = $query." OR";
            }
            $query = $query." $key = '$value'";
            $or = true;
        }
        if(sizeof($this->db->select($query)) > 0){
            return true;
        }
        return false;
    }

    /**
     * @param string $user
     * @param string $password
     * Vérifie si les informations d'authentification correspondent à un utilisateur
     * @return bool
     */
    public function login($user, $password){
        $password = hash('SHA512', $password.$this->config->get('gds'));
        $user = strtolower($user);
        $this->db->checkInjection([$user, $password]);
        $query = "SELECT * FROM users WHERE user = '$user' AND password = '$password'";
        if(sizeof($this->db->select($query)) > 0){
            return true;
        }
        else{
            return false;
        }
    }

}

?>