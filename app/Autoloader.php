<?php

/**
 * Class MyAutoloader
 */
class MyAutoloader
{

    /**
     * Défini quelle méthode de quelle classe doit être utilisée comme autoloader
     */
    public static function register(){
        spl_autoload_register(array(__CLASS__, 'autoloader'));
    }

    /**
     * @param $class
     * Appel la classe demandée
     */
    public static function autoloader($class){
        str_replace('\\', '/', $class);
        require '../app/'.$class.'.php';
    }

}

?>