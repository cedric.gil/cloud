<?php

/**
 * Class Twig
 */
class Twig
{

    /**
     * @var Twig
     */
    private static $_instance;

    /**
     * @var \Twig\Environment
     */
    private $twig;

    /**
     * @return Twig
     */
    public static function getInstance(){
        if(is_null(self::$_instance)){
            self::$_instance = New Twig();
        }
        return self::$_instance;
    }

    /**
     * Twig constructor.
     */
    private function __construct(){
        $loader = new Twig\Loader\FilesystemLoader('../pages');
        $this->twig = new Twig\Environment($loader, [
            'cache' => false
        ]);
    }

    /**
     * @param string $page
     * @param string $spage
     * @param array $paramsfromphpfile
     * Retourne la page demandée
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function getRender($page, $spage, $paramsfromphpfile){
        $params = $paramsfromphpfile;
        switch ($page){
            case 'home':
                return $this->twig->render('home.twig', $params);
            case 'my':
                switch ($spage){
                    default:
                        return $this->twig->render('my/home.twig', $params);
                }
            default:
                return $this->twig->render('404.twig');
        }
    }

    /**
     * @param string $page
     * Retourne le chemin du fichier PHP lié à la page
     * @return bool|string
     */
    public function getPhpPath($page, $spage){
        switch ($page){
            case 'home':
                return '../pages/php/home.php';
            case 'my':
                switch ($spage){
                    default:
                        return '../pages/my/php/home.php';
                }
            default:
                break;
        }
    }

}