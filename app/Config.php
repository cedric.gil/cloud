<?php

/**
 * Class Config
 */
class Config
{

    /**
     * @var Config
     */
    private static $_instance;

    /**
     * @var array
     */
    private $setting = [];

    /**
     * Crée une instance si celle-ci n'est pas déjà créé
     * @return Config
     */
    public static function getInstance(){
        if(is_null(self::$_instance)) {
            self::$_instance = new Config();
        }
        return self::$_instance;
    }

    /**
     * Récupère les informations du fichier config
     */
    private function __construct(){
        if($_SERVER['PHP_SELF'] === '/index.php'){
            $this->setting = require('../config/config.php');
        }
    }

    /**
     * @param string $key
     * Retourne l'information demandée
     * @return mixed|null
     */
    public function get($key){
        if(!isset($this->setting[$key])) {
            return null;
        }
        return $this->setting[$key];
    }
}

?>